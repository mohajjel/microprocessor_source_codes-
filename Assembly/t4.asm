%include "io.inc"
section .data
%ifndef _VS_
format_str db "Salam",10,0
%else
format_str db "Hello",10,0
%endif

section .text
extern _printf
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
     push dword format_str
	call _printf
	add esp,4
while:
    cmp eax, 5
    jnl endwhile
    mul eax; edx:eax=eax*eax
    mov ebx, eax
    shr ebx, 1; set cf
    jnc endwhile
    
endwhile:   
    xor eax, eax
    ret
