segment .text
;int linear_search(int * A, int x, int N)
global _linear_search

%ifndef __VS__
%include "io.inc"
section .data
A dd 1, 3, 5, 0
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    push dword 4
    push dword 5
    push A
    call _linear_search
    PRINT_DEC 4, eax      
    ;write your code here
    xor eax, eax
    ret
    
%endif    

_linear_search:
	push ebp
	mov ebp, esp
	push ebx
	mov eax, [ebp+8]
	mov ebx, [ebp+12]
     mov ecx, 0
	
L:
	cmp ebx, [eax+4*ecx]
	je NEXT
	inc ecx
	cmp ecx, [ebp+16]
	jl L
NEXT:
	mov eax, ecx
	pop ebx
	mov esp, ebp
	pop ebp
	ret
	