%include "io.inc"
section .data
L1 resb 4
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    GET_DEC 4, eax
    GET_DEC 4, [L1]
    add eax, [L1]
    PRINT_DEC 4, eax
    xor eax, eax
    ret