/*
* test_Keypad_polling.c
*
* Created: 5/12/2019 5:51:29 AM
* Author : morteza
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"

#define RESETBIT(x,bit) (x=x & ~(1<<bit))
#define SETBIT(x,bit) (x=x | (1<<bit))


char key[4][4]=
{
	{'1','2','3','a'},
	{'4','5','6','b'},
	{'7','8','9','c'},
	{'*','0','#','d'}};
	void detect_row(uint8_t c);
	ISR(INT0_vect){
		uint8_t c;
		//some key pressed
		if((PINB & 0b10000000) == 0){ //c3
			c=3;
			detect_row(c);
		}
		if((PINB & 0b01000000) == 0){ //c2
			c=2;
			detect_row(c);
		}
		if((PINB & 0b00100000) == 0){ //c1
			c=1;
			detect_row(c);
		}
		if((PINB & 0b00010000) == 0){ //c0
			c=0;
			detect_row(c);
		}
		DDRB =0b00001111;
		while((PINB & 0b11110000) != 0b11110000){

		}
		


	}

	void setup(){
		lcd_init(LCD_DISP_ON);
		DDRB =0b00001111;
		PORTB=0b11110000;
		SETBIT(GICR,INT0);
		SETBIT(MCUCR,ISC01);
		RESETBIT(MCUCR,ISC00);


		sei();
	}
	void detect_row(uint8_t c){
		uint8_t r;
		PORTB = 0b11110000;

		//c3 --> 10000000
		//c2 --> 01000000
		//c1 --> 00100000
		//c0 --> 00010000

		DDRB =	0b00000001;
		if((PINB & 1<<(c+4)) == 0){
			r=0;
			lcd_putc(key[r][c]);
			//c, r=0  -> print
		}
		DDRB =	0b00000010;
		if((PINB & 1<<(c+4)) == 0){
			r=1;
			lcd_putc(key[r][c]);
			//c, r=1  -> print

		}
		DDRB =	0b00000100;
		if((PINB & 1<<(c+4)) == 0){
			r=2;
			lcd_putc(key[r][c]);
			//c, r=2  -> print
		}
		DDRB =	0b00001000;
		if((PINB & 1<<(c+4)) == 0){
			r=3;
			lcd_putc(key[r][c]);
			//c, r=3  -> print
		}
	}
	int main(void)
	{
		
		setup();
		while (1)
		{
			
			


		}
	}

