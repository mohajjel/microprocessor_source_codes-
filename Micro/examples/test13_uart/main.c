/*
 * test13_uart_polling.c
 *
 * Created: 5/24/2019 11:40:05 AM
 * Author : morteza
 */ 

#include <avr/io.h>
#include <avr/interrupt.h> 
#include <util/delay.h>
#include <stdio.h>
 
#include "uart.h"



int my_uart_getc(){
	int c;
	do{
		c=uart_getc();
	}while(c & UART_NO_DATA );
	uart_putc(c);
	return c;
}

static FILE uart_inout = FDEV_SETUP_STREAM(uart_putc, my_uart_getc, _FDEV_SETUP_RW);
//static FILE uart_input = FDEV_SETUP_STREAM(my_uart_getc, NULL, _FDEV_SETUP_READ);


void setup(void){
	uart_init( UART_BAUD_SELECT(9600,F_CPU) ); 
	stdin = stdout = & uart_inout;
	//stdout = & uart_output;
	//stdin = & uart_input;
	sei();
}
int main(void)
{
	int d;
	setup();
    while (1) 
    {
		//uart_putc('C');
		//printf("salam\r\n");
		scanf("%d",&d);
		//char c= my_uart_getc();
		printf("resid:%d\r\n",d);
		//_delay_ms(1000);
    }
}

