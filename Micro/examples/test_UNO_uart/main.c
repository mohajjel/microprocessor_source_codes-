/*
* test_UNO_uart.c
*
* Created: 5/26/2019 4:18:26 PM
* Author : morteza
*/

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include "bit_tools.h"
#include "uart.h"

int my_uart_getc(){
	int c;
	do{
		c=uart_getc();
	}while(c & UART_NO_DATA );
	return c;
}
static FILE uart_inout = FDEV_SETUP_STREAM(uart_putc, my_uart_getc, _FDEV_SETUP_RW);
void setup(void){
	SETBIT(DDRB,5);
	RESETBIT(PORTB,5);
	uart_init(UART_BAUD_SELECT(9600,F_CPU));
	stdin = stdout = & uart_inout;
	sei();
}

int main(void)
{
	int x;
	setup();
	/* Replace with your application code */
	while (1)
	{
		TOGGLEBIT(PORTB,5);
		scanf("%c",&x);
		printf("SALAM%d\r\n",x);
		_delay_ms(1000);
	}
}

