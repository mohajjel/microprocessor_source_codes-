/*
 * test13_uart_polling.c
 *
 * Created: 5/24/2019 11:40:05 AM
 * Author : morteza
 */ 

#include <avr/io.h>
#include <avr/interrupt.h> 
#include <util/delay.h>
#include <stdio.h>
 
#include "uart.h"


#define ADC_VREF_TYPE ((0<<REFS1) | (0<<REFS0) | (0<<ADLAR))

// Read the AD conversion result
unsigned int read_adc(unsigned char adc_input)
{
	ADMUX=adc_input | ADC_VREF_TYPE;
	// Delay needed for the stabilization of the ADC input voltage
	_delay_us(10);
	// Start the AD conversion
	ADCSRA|=(1<<ADSC);
	// Wait for the AD conversion to complete
	while ((ADCSRA & (1<<ADIF))==0);
	ADCSRA|=(1<<ADIF);
	return ADCW;
}

int my_uart_getc(){
	int c;
	do{
		c=uart_getc();
	}while(c & UART_NO_DATA );
	return c;
}

static FILE uart_inout = FDEV_SETUP_STREAM(uart_putc, my_uart_getc, _FDEV_SETUP_RW);
//static FILE uart_input = FDEV_SETUP_STREAM(my_uart_getc, NULL, _FDEV_SETUP_READ);


void setup(void){
	uart_init( UART_BAUD_SELECT(9600,F_CPU) ); 
	stdin = stdout = & uart_inout;
	
	// Analog Comparator initialization
	// Analog Comparator: Off
	// The Analog Comparator's positive input is
	// connected to the AIN0 pin
	// The Analog Comparator's negative input is
	// connected to the AIN1 pin
	ACSR=(1<<ACD) | (0<<ACBG) | (0<<ACO) | (0<<ACI) | (0<<ACIE) | (0<<ACIC) | (0<<ACIS1) | (0<<ACIS0);

	// ADC initialization
	// ADC Clock frequency: 125.000 kHz
	// ADC Voltage Reference: AREF pin
	// ADC Auto Trigger Source: ADC Stopped
	ADMUX=ADC_VREF_TYPE;
	ADCSRA=(1<<ADEN) | (0<<ADSC) | (0<<ADATE) | (0<<ADIF) | (0<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);
	SFIOR=(0<<ADTS2) | (0<<ADTS1) | (0<<ADTS0);



	sei();
}
int main(void)
{
	int d;
	float tf;
	setup();
    while (1) 
    {
		d = read_adc(0);
		tf =(float)d*500/1023;
		printf("value=%.2f\r\n",tf);
		_delay_ms(1000);
    }
}

