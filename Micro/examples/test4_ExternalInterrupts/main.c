/*
* test_ExternalInterrupts.c
*
* Created: 5/11/2019 6:13:17 AM
* Author : morteza
*/

#include <avr/io.h>
#include <avr/interrupt.h>
ISR(INT0_vect){
	static uint8_t flag = 0;
	PORTB = flag ? 0xff : 0x00;
	flag = flag ? 0 : 1;
}
void setup(void){
	//set PD2 as input with pull up
	DDRD=0<<PD2;
	PORTD=1<<PD2;

	DDRB=0xff;

	GICR = 1<<INT0 ;
	MCUCR = 1<<ISC01 | 0<<ISC00;

	sei();
}

int main(void)
{
	setup();
	/* Replace with your application code */
	while (1)
	{
	}
}

