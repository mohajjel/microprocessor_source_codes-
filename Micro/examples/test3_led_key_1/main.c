/*
 * test_led_key_1.c
 *
 * Created: 4/28/2019 4:32:31 PM
 * Author : morteza
 */ 

#include <avr/io.h>

//#define SET_BIT(x,b) (x | 1<<b)
//#define RESET_BIT(x,b) (x & ~(1<<b))

int main(void)
{
	PORTA=0x01;
	DDRA=0x02;
	while(1){
		if(PINA & 0x01)
			PORTA= PORTA | 0x02; // 00000010  SET_BIT(PORTA,1)
		else
			PORTA= PORTA & ~(0x02); // 11111101	
	}

}

