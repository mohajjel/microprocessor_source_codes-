/*
* test_timer1.c
*
* Created: 5/17/2019 6:31:33 AM
* Author : morteza
*/


#include <avr/io.h>
#include <avr/interrupt.h>

#include "bit_tools.h"

typedef uint64_t timer_t;


static timer_t n_millis=0;
timer_t millis(void){
	timer_t t;
	cli();
		t=n_millis;
	sei();
	return t;
}

void setup(void){
	TCCR1A=(0<<COM1A1) | (0<<COM1A0) | (0<<COM1B1) | (0<<COM1B0) | (0<<WGM11) | (0<<WGM10);
	TCCR1B=(0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (1<<WGM12) | (0<<CS12) | (0<<CS11) | (1<<CS10);
	TCNT1H=0x00;
	TCNT1L=0x00;
	ICR1H=0x00;
	ICR1L=0x00;
	OCR1AH=0x3E; //15999
	OCR1AL=0x7F;
	OCR1BH=0x00;
	OCR1BL=0x00;

	SETBIT(TIMSK,OCIE1A);

	SETBIT(DDRB,DDB0);
	SETBIT(DDRB,DDB1);

	sei();
}
int main(void)
{
	timer_t t2=0,t1=0;
	
	setup();
	/* Replace with your application code */
	while (1)
	{
		if(millis()-t1>=500){
			TOGGLEBIT(PORTB,PB0);
			t1=millis();
		}
		if(millis()-t2>=700){
			TOGGLEBIT(PORTB,PB1);
			t2=millis();
		}
	}
}

ISR(TIMER1_COMPA_vect){
	TOGGLEBIT(PORTB,PB0);
	n_millis++;
	
}