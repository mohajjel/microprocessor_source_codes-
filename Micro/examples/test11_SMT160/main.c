/*
* test_timer1.c
*
* Created: 5/17/2019 6:31:33 AM
* Author : morteza
*/


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "lcd.h"
#include <stdbool.h>
#include <stdio.h>

#include "bit_tools.h"
uint8_t T1,T2;
bool edge=false;

static FILE lcd_output = FDEV_SETUP_STREAM(lcd_putc, NULL, _FDEV_SETUP_WRITE);

void setup(void){
	//Timer 0
	//TCCR0
	RESETBIT(TCCR0,WGM00);
	RESETBIT(TCCR0,WGM01);

	RESETBIT(TCCR0,COM00);
	RESETBIT(TCCR0,COM01);

	RESETBIT(TCCR0,CS02);
	SETBIT(TCCR0,CS01);
	RESETBIT(TCCR0,CS00);
	

	
	DDRD=0<<PD2;

	SETBIT(GICR,INT0) ;
	
	SETBIT(MCUCR,ISC01);
	RESETBIT(MCUCR,ISC00);

	lcd_init(LCD_DISP_ON);
	stdout = & lcd_output;
	sei();
}
int main(void)
{
	float dc, t;
	setup();
	/* Replace with your application code */
	while (1)
	{
	      _delay_ms(1000);
	      dc=(float)T1/(T1+T2);
	      t=(dc-0.32)/0.0047;
	      lcd_clrscr();
		  printf("%.2f \xDF C",dc);
	}
}

ISR(INT0_vect){
	if(edge)
	{
		T2=TCNT0;
		TCNT0=0;
		MCUCR=0x02;
		edge=false;
	}else
	{
		T1=TCNT0;
		TCNT0=0;
		MCUCR=0x03;
		edge=true;
	}

}