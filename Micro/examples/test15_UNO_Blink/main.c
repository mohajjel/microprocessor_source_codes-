/*
 * test15_UNO_Blink.c
 *
 * Created: 5/25/2019 3:51:43 PM
 * Author : morteza
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "bit_tools.h"
#include "uart.h"


void setup(){
	SETBIT(DDRB,5);
	uart_init( UART_BAUD_SELECT(9600,F_CPU) );
	sei();
}
int main(void)
{
	setup();
	
    /* Replace with your application code */
    while (1) 
    {
		TOGGLEBIT(PORTB,5);
		uart_putc('c');
		_delay_ms(500);
    }
}

