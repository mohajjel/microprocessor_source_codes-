/*
* test_timer1.c
*
* Created: 5/17/2019 6:31:33 AM
* Author : morteza
*/


#include <avr/io.h>
#include <avr/interrupt.h>

#include "bit_tools.h"

void setup(void){
	//Timer 0
	//TCCR0
	RESETBIT(TCCR0,WGM00);
	SETBIT(TCCR0,WGM01);

	SETBIT(TCCR0,COM00);
	RESETBIT(TCCR0,COM01);

	SETBIT(TCCR0,CS02);
	RESETBIT(TCCR0,CS01);
	SETBIT(TCCR0,CS00);
	
	OCR0=100;

	//OC0
	SETBIT(DDRB,DDB3);
	RESETBIT(PORTB,PB3);


}
int main(void)
{
	setup();
	/* Replace with your application code */
	while (1)
	{
	}
}