/*
 * test_ExternalInterrupts2.c
 *
 * Created: 5/11/2019 8:30:05 AM
 * Author : morteza
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#define RESETBIT(x,bit) (x=x & ~(1<<bit)) 
#define SETBIT(x,bit) (x=x | (1<<bit))
//11111110
//00000001
ISR(INT0_vect){
	static bool flag=false;
	if(flag){
		RESETBIT(PORTD,1);
		flag=false;
	}else{
		SETBIT(PORTD,1);
		flag=true;
	}
}
 
void setup(){
	
	//interrupt INT0
	GICR|= 1<<INT0;
	MCUCR|= 1<<ISC01 | 0<<ISC00;
	RESETBIT(DDRD,2);
	SETBIT(PORTD,2);

	//led
	SETBIT(DDRD,1);
	RESETBIT(PORTD,1);

	sei();
}


int main(void)
{
	setup();
    /* Replace with your application code */
    while (1) 
    {
    }
}

