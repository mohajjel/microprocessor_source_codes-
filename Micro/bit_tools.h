#ifndef __BIT_TOOLS_H__
#define __BIT_TOOLS_H__

#define RESETBIT(x,bit) (x=x & ~(1<<bit))
#define SETBIT(x,bit) (x=x | (1<<bit))
#define TOGGLEBIT(x,bit) (x= x ^ (1<<bit))

#endif